# Stage_DSDA

----------------------------------- Bubbles ---------------------------------------------------------------------------------------------

Récupère des donnés CSV du fichier " translation_count.csv "

puis les transforms en un graphs à bulles . 

les données sont le nombre d'élement traduit enver chaque langue pour une langue précise
séléctioné à l'aide d'un drop down list.


ce dernier utilise " d3js" version 4 et "jquery" . 



---------------------------------- RecupeDonnée ------------------------------------------------------------------------------------------

Comporte le scripts javascript pour la récupération de donnée direction en lançant une requète SPARQL.

On peut changer la valuer de "myEndPoint" pour que ça corresponde au serveur convenable , à condition que le CORS soit autorisé.



---------------------------------- rdfToGraph --------------------------------------------------------------------------------------------

Ce dernier ce sert du même code de RecupDonnée , hors on peut parametré notre requête directement apartir d'un input text

ainsi que de choisir la langue ou on veut chercher ce qu'on veut . 

après la récupération on transform ce qu'on a récuperé en un trableau de triplets : subject , predicate , object .

convenable avec la représentation RDF .

ainsi à l'aide de d3js on utilise cela pour créer un graph représentant le subject (donnéer rechercher) et ces liens avec d'autre object . 

Concernant les données rechercher , il faut entré l'indicateur our bien l'id précis de cela , par exemple "mouse__Noun__1" si on cherche "mouse".

 RDFtoGraph.html se charge de tout cela . 

-------------------------------- autorisation du CORS -------------------------------------------------------------------------------------

1 - On se met sur notre instance Admin (Conductor) : http://{your-instance-hostname}:[port]/conductor . 

2 - On va sur "Web Application Server"->"Virtual Domains & Directories" .

3 - On étend l'interface pour "Default Web Site" . 

4 - Localise /sparql .

5 - edit : on entre "*" sur l'input Cross-Origin Ressource Sharing . 
